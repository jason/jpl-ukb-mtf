library(tidyverse)

df_fastfirst <- read.csv("../../raw_data/biobank_dload/ukb671072_FAST_FIRST.csv", stringsAsFactors = FALSE)

lookup <- read.csv("../FAST_FIRST_idp_lookup.csv",stringsAsFactors = FALSE)
names(df_fastfirst)
#only want t 2_0
df_fastfirst<-df_fastfirst[,c(1,seq(2,306,2))]
names(df_fastfirst)

idpnames <- c()

for (x in names(df_fastfirst)[seq(2,154)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}

for (x in names(df_fastfirst)[seq(2,154)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_fastfirst)<-c("eid",idpnames)
#save
save(df_fastfirst, file="../../derivatives/df_FASTFIRST.rda")
rm(df_fastfirst, lookup)

#### Freesurfer ASEG ####
df_freesurfer_aseg <- read.csv("../../raw_data/biobank_dload/ukb672010_FreesurferASEG.csv", stringsAsFactors = FALSE)
lookup <- read.csv("../FreesurferASEG_idp_lookup.csv",stringsAsFactors = FALSE)

#only want t 2_0
df_freesurfer_aseg<-df_freesurfer_aseg[,c(1,seq(2,198,2))]
names(df_freesurfer_aseg)

idpnames <- c()

for (x in names(df_freesurfer_aseg)[seq(2,100)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}
for (x in names(df_freesurfer_aseg)[seq(2,100)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_freesurfer_aseg)<-c("eid",idpnames)
#save
save(df_freesurfer_aseg, file="../../derivatives/df_FreesurferASEG.rda")
rm(df_freesurfer_aseg, lookup)


#### Freesurfer DKT ####
df_freesurfer_dkt <- read.csv("../../raw_data/biobank_dload/ukb672010_FreesurferDKT.csv", stringsAsFactors = FALSE)
lookup <- read.csv("../FreesurferDKT_idp_lookup.csv",stringsAsFactors = FALSE)

#only want t 2_0
df_freesurfer_dkt<-df_freesurfer_dkt[,c(1,seq(2,372,2))]
names(df_freesurfer_dkt)

idpnames <- c()

for (x in names(df_freesurfer_dkt)[seq(2,187)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}
for (x in names(df_freesurfer_dkt)[seq(2,187)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_freesurfer_dkt)<-c("eid",idpnames)
#save
save(df_freesurfer_dkt, file="../../derivatives/df_FreesurferDKT.rda")
rm(df_freesurfer_dkt, lookup)



#### TBSS WM ####
df_tbss <- read.csv("../../raw_data/biobank_dload/ukb671072_dMRI.csv", stringsAsFactors = FALSE)
names(df_tbss)
df_tbss <- df_tbss[,c(1,seq(2,578,2))]
names(df_tbss)

lookup<-read.csv("../wm_idp_lookup.csv", stringsAsFactors = FALSE)

idpnames <- c()

for (x in names(df_tbss)[seq(2,290)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}
for (x in names(df_tbss)[seq(2,290)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_tbss)<-c("eid",idpnames)
names(df_tbss)
save(df_tbss, file="../../derivatives/df_wm.rda")
rm(df_tbss, lookup)

#### siena #### 
df_siena <- read.csv("../../raw_data/biobank_dload/ukb671072_siena.csv", stringsAsFactors = FALSE)
names(df_siena)
df_siena <- df_siena[,c(1,seq(2,22,2))]
names(df_siena)

lookup <- read.csv("../siena_idp_lookup.csv", stringsAsFactors = FALSE)

idpnames <- c()

for (x in names(df_siena)[seq(2,12)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}
for (x in names(df_siena)[seq(2,12)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_siena)<-c("eid",idpnames)
names(df_siena)
save(df_siena, file="../../derivatives/df_siena.rda")
rm(df_siena, lookup)

#### covariates ####
df_covar <- read.csv("../../raw_data/biobank_dload/ukb671072_covariates.csv", stringsAsFactors = FALSE)
df_covar$eid <- as.character(df_covar$eid)
names(df_covar)

#first lets separate the education fields, those need separate processing
#845 = age completed education
#6138 - qualifications
df_education <- df_covar[,c(1,seq(6,32))]
names(df_education)
write.csv(df_education, file="../../derivatives/education_years_qualifications.csv", row.names = FALSE)

#then drop these
df_covar <- df_covar[,-seq(6,32)]
names(df_covar)
rm(df_education)

#get t2
df_covar <- df_covar[,c(1,seq(4,28,2))]
names(df_covar)

#25746 is dmri outlier slices which is redundant with 24456 eddy slices, drop
df_covar <- df_covar[,-10]
names(df_covar)

#get extra confounds (motion, eddy, t2flair used)
df_extra_covar <- read.csv("../../raw_data/biobank_dload/ukb673825_motion_eddy_flair.csv", stringsAsFactors = FALSE)
df_extra_covar$eid <- as.character(df_extra_covar$eid)
names(df_extra_covar)

#get t2
df_extra_covar <- df_extra_covar[,c(1,2,4,6)]
names(df_extra_covar)

#merge
df_covar <- list(df_covar, df_extra_covar) %>% reduce(left_join, by="eid")
names(df_covar)
rm(df_extra_covar)

lookup <- read.csv("../covariates_lookup.csv", stringsAsFactors = FALSE)

idpnames <- c()

for (x in names(df_covar)[seq(2,16)]){
  print(x)
  print(lookup[which(lookup$idp==x),]$idpname)
}
for (x in names(df_covar)[seq(2,16)]){
  idpnames <- append(idpnames, lookup[which(lookup$idp==x),]$idpname)
  print(lookup[which(lookup$idp==x),]$idpname)
}

names(df_covar)<-c("eid",idpnames)
names(df_covar)
save(df_covar, file="../../derivatives/df_covariates.rda")

  
  